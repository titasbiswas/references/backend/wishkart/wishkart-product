package com.wishkat.product.models;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Document
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Product {
	

	@Id
	String id;
	String name;
	String cat;
	String brand;
	String shortDesc;
	String desc;
	String imageUrl;
	BigDecimal rate;
	BigDecimal discountPrcntg;
	BigDecimal amount;
	List<ProductSpec> specs = new ArrayList<>();
	List<ProductRivews> reviews = new ArrayList<>();
}
