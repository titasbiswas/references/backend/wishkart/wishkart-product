package com.wishkat.product.models;

import java.util.Date;

import lombok.Data;

@Data
public class ProductRivews {

	String user;
	String review;
	Date reviewTime;
}
