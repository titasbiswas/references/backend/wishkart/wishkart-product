package com.wishkat.product.models;

import lombok.Data;

@Data
public class ProductSpec {
	
	String specName;
	String specValue;
}
