package com.wishkat.product.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wishkat.product.exceptionHandling.EntityNotFoundException;
import com.wishkat.product.models.Product;
import com.wishkat.product.repositories.ProductRepository;
import com.wishkat.product.services.ProductService;

@RestController
@RequestMapping("/products")
public class ProductController {
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private ProductRepository productRepo;
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Product>> getAllProducts(){
		return  ResponseEntity.ok(productService.getAll());
	}
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE , produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Product> addProduct(@RequestBody Product product) {
		return ResponseEntity.ok(productService.addProduct(product));
	}
	
	@PutMapping(value="/updateprice", consumes = MediaType.APPLICATION_JSON_VALUE , produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Product> updateProductPrice(@RequestBody Product product) throws EntityNotFoundException{
		Product updatedProduct;
		Optional<Product> optProduct = productRepo.findById(product.getId());
		if(optProduct.isPresent()) {
			updatedProduct = optProduct.get();
			updatedProduct.setAmount(product.getAmount());
			updatedProduct.setDiscountPrcntg(product.getDiscountPrcntg());
			updatedProduct.setRate(product.getRate());
			return ResponseEntity.ok(productRepo.save(updatedProduct));
		}
		else throw new EntityNotFoundException(Product.class, "Id", product.getId().toString());
	}
	
	@DeleteMapping(value="/{id}")
	void deleteProduct(@PathVariable("id") String productId) {
		Optional<Product> optProduct = productRepo.findById(productId);
		optProduct.ifPresent(p -> {
			productRepo.delete(p);
		});
	}

}
