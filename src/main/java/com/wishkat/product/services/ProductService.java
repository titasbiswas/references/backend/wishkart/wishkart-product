package com.wishkat.product.services;

import java.util.List;
import java.util.Optional;

import com.wishkat.product.models.Product;

public interface ProductService {
	
	List<Product> getAll();
	Optional<Product> getById(String id);
	Product addProduct(Product product);
}
