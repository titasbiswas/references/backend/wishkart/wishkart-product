package com.wishkat.product.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wishkat.product.models.Product;
import com.wishkat.product.repositories.ProductRepository;

@Service
public class ProductServiceImpl implements ProductService {
	
	@Autowired
	private ProductRepository productRepo;

	@Override
	public List<Product> getAll() {
		return productRepo.findAll();
	}

	@Override
	public Optional<Product> getById(String id) {
		return productRepo.findById(id);
	}

	@Override
	public Product addProduct(Product product) {
		return productRepo.save(product);
	}

}
