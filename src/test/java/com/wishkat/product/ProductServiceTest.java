package com.wishkat.product;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.wishkat.product.models.Product;
import com.wishkat.product.models.ProductRivews;
import com.wishkat.product.models.ProductSpec;
import com.wishkat.product.services.ProductService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductServiceTest {
	
	@Autowired
	private ProductService productService;
	
	@Test
	public void testProductSave() {
		List<ProductSpec> specs = new ArrayList<>();
		ProductSpec spec = new ProductSpec();
		spec.setSpecName("Wireless communication technologies");
		spec.setSpecValue("Bluetooth;WiFi Hotspot");
		specs.add(spec);
		spec = new ProductSpec();
		spec.setSpecName("Battery Power Rating");
		spec.setSpecValue("3300 Mah");
		specs.add(spec);
		spec = new ProductSpec();
		spec.setSpecName("Whats in the box");
		spec.setSpecValue("Handset, Adapter, USB cable, Warranty Card, User guide and SIM insertion tool");
		specs.add(spec);
		
		List<ProductRivews> productRivews = new ArrayList<>();
		ProductRivews rivews = new ProductRivews();
		rivews.setUser("PRIYANKA");
		rivews.setReview("Price range wise good. Processor, "
				+ "touch, speed everything is nice. But camera could be better..as "
				+ "i have used mi phones for a long time since redmi2 was launched. "
				+ "This phone's camera is not like the previous ones.");
		rivews.setReviewTime(new Date(System.currentTimeMillis()));
		productRivews.add(rivews);
		rivews = new ProductRivews();
		rivews.setUser("Ajesh");
		rivews.setReview("Poor battery, MiUi was not impressive. Too many default apps "
				+ "which cannot be removed.");
		rivews.setReviewTime(new Date(System.currentTimeMillis()));
		productRivews.add(rivews);
		
		Product product = new Product();
		product.setId("5b818c2c5c424535dde2f27c");
		product.setBrand("Xiaomi");
		product.setCat("electronics");
		product.setDesc("The 14.4cm(5.7) display with rounded corner design gives an ergonomically"
				+ " curved for better hand feel. Turn on the display, delight your world. "
				+ "Qualcomm Snapdragon 450 octa-core processor with 14nm FinFET technology; coupled with "
				+ "MIUI 9 system level optimisations enables low power consumption for a longer battery"
				+ " life. 12MP high quality camera. Capture great photos in different lighting conditions."
				+ " Gorgeous portraits with automatically enhanced facial features using Beautify 3.0.");
		product.setImageUrl("https://images-na.ssl-images-amazon.com/images/I/81ORlGLUzWL._SL1500_.jpg");
		product.setName("Redmi 5 (Gold, 16GB)");
		product.setReviews(productRivews);
		product.setShortDesc("14.4 cm (5.7) 18:9 Full screen display, 2.5D curved glass");
		product.setSpecs(specs);
		
		Product newProduct = productService.addProduct(product);
		System.out.println(newProduct.getId());
		assertNotNull(newProduct.getId());
		
		
		//Saving a second product
		List<ProductSpec> specs2 = new ArrayList<>();
		ProductSpec spec2 = new ProductSpec();
		spec2.setSpecName("Model Number");
		spec2.setSpecValue("607");
		specs2.add(spec2);
		spec2 = new ProductSpec();
		spec2.setSpecName("Material Type(s)");
		spec2.setSpecValue("Plastic");
		specs2.add(spec2);
		spec2 = new ProductSpec();
		spec2.setSpecName("Product Dimensions");
		spec2.setSpecValue("2.5 x 2.5 x 2.5 cm");
		specs2.add(spec2);
		
		List<ProductRivews> productRivews2 = new ArrayList<>();
		ProductRivews rivews2 = new ProductRivews();
		rivews2.setUser("Vivon Pereira");
		rivews2.setReview("Its really a very good toy for babies (kids between 1 to 3 yrs). "
				+ "The baby whom I gifted it, loved to play with it all the time.");
		rivews2.setReviewTime(new Date(System.currentTimeMillis()));
		productRivews2.add(rivews2);
		rivews2 = new ProductRivews();
		rivews2.setUser("Customer_SM");
		rivews2.setReview("An very old game for the kids, and they still enjoy it.. "
				+ "Sometimes the ring goes on the stump and sometimes the rings becomes her bangles. "
				+ "My daughter just loves it.");
		rivews2.setReviewTime(new Date(System.currentTimeMillis()));
		productRivews2.add(rivews2);
		
		Product product2 = new Product();
		product2.setId("5b7e2e8d5c42451f06b024ec");
		product2.setBrand("Little's");
		product2.setCat("toys");
		product2.setDesc("Little's junior ring is an attractive educational toy which helps "
				+ "your baby recognize different colours and sizes while having fun stacking them in orde");
		product2.setImageUrl("https://images-na.ssl-images-amazon.com/images/I/61keEX3ze%2BL._SL1500_.jpg");
		product2.setName("Little's Junior Ring (Multicolour)");
		product2.setReviews(productRivews2);
		product2.setShortDesc("Sorting, Stacking & Plugging Toy");
		product2.setSpecs(specs2);
		
		Product newProduct2 = productService.addProduct(product2);
		System.out.println(newProduct2.getId());
		assertNotNull(newProduct2.getId());
	}

}
